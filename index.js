// importe module express
const express = require('express');
const connection = require('./conf/db');
// Importation et configuration de dotenv
require('dotenv').config();
// Import du module CORS
const cors = require('cors');
// Objet qui permet de gérer les requests & responses du serveur 
// vers le client
const app = express();
// défini port d'écoute
const port = 3000;

// Configure le module CORS
app.use(cors({
    origin: 'http://loalhost:8080'
}));

// Configure l'application pour parser les requêtes au format JSON
app.use(express.json())

// //Configure l'application pour parser encodées au format URL
app.use(
    express.urlencoded({
        extended: true,
    })
);

// Définition d'un endpoint '/' grâce à GET
app.get('/', (req, res) => {
    res.send('Bienvenue chez Mille Pattes !');
});

// Création d'un endpoint '/member' grâce à GET
app.get('/members', (req, res) => {
    connection.execute('SELECT * FROM member', (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la récupération de la liste des membres')
        } else {
            res.send(results)
        }
    });
});

// Création d'un endpoint '/member/id' grâce à GET
app.get('/members/:id', (req, res) => {
    const id = req.params.id;
    connection.execute('SELECT * FROM member WHERE member_id = ?', [id], (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la récupération du membre');
        } else {
            res.json(results)
        }
    })
})

// Création d'un endpoint '/member/post/occupation' grâce à GET

// Création d'un endpoint '/members' pour créer un objet grâce à la méthode POST
app.post('/members', (req, res) => {
    const { lastname, firstname, birthdate, occupation, date_employment } = req.body;
    connection.execute('INSERT INTO member (lastname, firstname, birthdate, occupation, date_employment) VALUES (?, ?, ?, ?, ?)', [lastname, firstname, birthdate, occupation, date_employment], (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la création du membre')
        } else {
            res.status(201).send(`Création du membre avec l'ID ${results.insertId}`)
        }
    });
});

// Création d'un endpoint '/members/id' pour mettre à jour un objet grâce à la méthode PUT
app.put('/members/:id', (req,res) => {
    const { lastname, firstname, birthdate, occupation, date_employment } = req.body;
    const id = req.params.id;
    connection.execute('UPDATE member SET lastname = ?, firstname = ?, birthdate = ?, occupation = ?, date_employment = ? WHERE member_id = ?', [lastname, firstname, birthdate, occupation, date_employment, id], (err, results) => {
        if (err) {
            res.status(500).send(`Erreur lors de la mise à jour du membre`);
        } else {
            res.send(`Membre mis à jour`);
        }
    });
});

// Création d'un endpoint '/members/id' pour supprimer un objet grâce à la méthode DELETE
app.delete('/members/:id', (req, res) => {
    const id = req.params.id;
    connection .execute('DELETE FROM member WHERE member_id = ?', [id], (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la suppression du membre')
        } else {
            res.status(200).send('Suppression réussie')
        }
    });
});

// Création d'un endpoint '/animals' grâce à GET
app.get('/animals', (req, res) => {
    connection.execute('SELECT * FROM animal', (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la  récupération de la liste des animaux')
        } else {
            res.send(results)
        }
    });
});

// Création d'un endpoint '/animals' grâce à la méthode POST
app.post('/animals', (req, res) => {
    const { name, birthdate, status, owner_lastname, owner_firstname, owner_phone_number } = req.body;
    connection.execute('INSERT INTO animal (name, birthdate, status, owner_lastname, owner_firstname, owner_phone_number) VALUES (?, ?, ?, ?, ?, ?)', [name, birthdate, status, owner_lastname, owner_firstname, owner_phone_number], (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la création de l\'animal')
        } else {
            res.status(201).send(`Création de l'animal avec l'ID ${results.insertId}`)
        }
    });
});

// Création d'un endpoint '/drugs' grâce à GET
app.get('/drugs', (req, res) => {
    connection.execute('SELECT * FROM drug', (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la récupération de la liste des médeicaments')
        } else {
            res.send(results)
        }
    });
});

// Création d'un endpoint '/drugs' grâce à la méthode POST
app.post('/drugs', (req, res) => {
    const { name, brand, unit_price_excl_taxes, stock_quantity } = req.body;
    connection.execute('INSERT INTO drug VALUES name = ?, brand = ?, unit_price_excl_taxes = ?, stock_quantity = ?', [name, brand, unit_price_excl_taxes, stock_quantity], (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la cr"ation du médicament')
        } else {
            res.send(`Création du médicament avec l'ID ${results.insertId}`)
        }
    });
});

// Création d'un endpoint '/drugs/id' pour mettre à jour un objet grâce à la méthode PUT
app.put('/drugs/:id', (req, res) => {
    const id = req.params.id;
    const { name, brand, unit_price_excl_taxes, stock_quantity } = req.body;
    connection.execute('UPDATE drug SET name = ?, brand = ?, unit_price_excl_taxes = ?, stock_quantity = ? WHERE drug_id = ?', [name, brand, unit_price_excl_taxes, stock_quantity, id], (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la mise à jour du médicament')
        } else {
            res.status(200).send('Mise à jour réussie')
        }
    });
});

// démarrage du serveur sur le port défini
app.listen(port, () => {
    console.log(`Ecoute sur le port ${port}`)
});